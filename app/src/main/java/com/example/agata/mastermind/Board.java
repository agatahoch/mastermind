package com.example.agata.mastermind;

import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;


public class Board {
    public void placeButtons(final Button button, int i, int j, RelativeLayout gameLayout){
        button.setId(R.id.button + j + 1 + (i * 4));
        button.setBackgroundResource(R.drawable.grey_button);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(95, 95);
        params.setMargins(695-(j*140),1470-(i*150),10,0);
        gameLayout.addView(button,params);
    }

    public void placeColorButtons(final Button colorButton, int i, RelativeLayout gameLayout){
        colorButton.setId(R.id.colorButton + i);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(95, 95);
        params.setMargins(90,130+(i*150),10,10);
        int color = setColor(colorButton);
        colorButton.setBackgroundResource(color);
        gameLayout.addView(colorButton,params);
    }

    private int setColor(Button colorButton){
        int id = colorButton.getId();
        if(id == R.id.colorButton){
            return R.drawable.red_button;
        } else if(id == R.id.colorButton + 1){
            return R.drawable.yellow_button;
        } else if(id == R.id.colorButton + 2){
            return R.drawable.blue_button;
        } else if(id == R.id.colorButton + 3){
            return R.drawable.green_button;
        } else if(id == R.id.colorButton + 4){
            return R.drawable.orange_button;
        } else{
            return R.drawable.purple_button;
        }
    }

    public void placeMiniButtons(Button miniButton, int i, int j, int k, RelativeLayout gameLayout) {
        miniButton.setBackgroundResource(R.drawable.grey_button);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(40, 40);
        params.setMargins(890 + (k*45), 1520 - i*150 - j*45 , 100, 100);
        gameLayout.addView(miniButton, params);
    }

    public int getColorOfButton(int id){
        if(id == R.id.colorButton){
            return R.drawable.red_button;
        } else if(id == R.id.colorButton + 1){
            return R.drawable.yellow_button;
        } else if(id == R.id.colorButton + 2){
            return R.drawable.blue_button;
        } else if(id == R.id.colorButton + 3){
            return R.drawable.green_button;
        } else if(id == R.id.colorButton + 4){
            return R.drawable.orange_button;
        } else if(id == R.id.colorButton + 5) {
            return R.drawable.purple_button;
        }
            return R.drawable.grey_button;
    }
}

