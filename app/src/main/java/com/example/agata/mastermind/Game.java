package com.example.agata.mastermind;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class Game extends Activity{
    int value = 0;
    int count = 0;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);

        final Button[][] boardButtons = new Button[10][4];
        final GameController gameController = new GameController();


        final RelativeLayout gameLayout = (RelativeLayout) findViewById(R.id.game);

        super.setContentView(gameLayout);


        final Button[][][] checkButtons = new Button[10][2][2];

        final Board board = new Board();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 4; j++) {
                final Button button = new Button(this);
                board.placeButtons(button, i, j, gameLayout);
                boardButtons[i][j] = button;
                boardButtons[i][j].setEnabled(false);
                boardButtons[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(value!=0) {
                            button.setId(value);
                            button.setBackgroundResource(value);
                        }
                    }
                });
            }
        }

        final ArrayList<Integer> match = gameController.generateMatch();

        BoardTools.changeRow(count,true,boardButtons);

        for (int i = 0; i < 6; i++) {
            final Button colorButton = new Button(this);
            colorButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   value = board.getColorOfButton(colorButton.getId());
                }
            });
            board.placeColorButtons(colorButton,i,gameLayout);
        }

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 2; j++) {
                for(int k=0; k < 2;k++){
                    Button miniButton = new Button(this);
                    board.placeMiniButtons(miniButton, i,j,k, gameLayout);
                    checkButtons[i][j][k] = miniButton;
                }
            }
        }

        Button check = (Button) findViewById(R.id.check);
        check.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Pair<Integer,Integer> pair = gameController.checkRow(boardButtons[count],match);
                BoardTools.gradeRow(pair,checkButtons[count]);
                if(pair.first==4){
                    Toast.makeText(getApplicationContext(), "You win!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(count==9) {
                    Toast.makeText(getApplicationContext(), "You loose!", Toast.LENGTH_SHORT).show();
                    for(int i=0;i<4;i++){
                        showGeneratedCode(match,gameLayout,i);
                        showText(gameLayout);
                    }
                    return;
                }
                BoardTools.changeRow(count,false,boardButtons);
                count += 1;
                BoardTools.changeRow(count,true,boardButtons);
            }
        });

        Button quit = (Button) findViewById(R.id.quit);
        quit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(),Menu.class);
                startActivityForResult(myIntent, 0);
          }
        });
    }
    public final void showGeneratedCode(ArrayList<Integer> match,RelativeLayout
            gameLayout,int i){
        Button matchButton = new Button(this);
        Button[] matchButtons = new Button[4];
        matchButtons[i] = matchButton;
        matchButtons[i].setBackgroundResource(match.get(i));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(70, 70);
        params.setMargins(707-(i*140),18,10,0);
        gameLayout.addView(matchButton,params);
    }
    public final void showText(RelativeLayout gameLayout){
        TextView text = new TextView(this);
        text.setText("CODE: ");
        text.setPadding(90,25,0,0);
        gameLayout.addView(text);
    }
}

