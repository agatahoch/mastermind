package com.example.agata.mastermind;

import android.util.Pair;
import android.widget.Button;
import java.util.ArrayList;

public class GameController{
    public ArrayList<Integer> generateMatch(){
        ArrayList<Integer> match = new ArrayList<>();
        for(int i=0;i<4;i++){
            int color = randomColor();
            if(match.contains(color)){
                i -= 1;
            }
            else{
                match.add(color);
            }
        }
        return match;
    }
    private int randomColor(){
        int number = (int) (Math.random() * 6);
        if(number == 0){
            return R.drawable.red_button;
        } else if(number ==  1){
            return R.drawable.yellow_button;
        } else if(number ==  2){
            return R.drawable.blue_button;
        } else if(number ==  3){
            return R.drawable.green_button;
        } else if(number ==  4){
            return R.drawable.orange_button;
        } else if(number ==  5) {
            return R.drawable.purple_button;
        }
        return 0;
    }
    public Pair<Integer,Integer> checkRow(Button[] boardButtons, ArrayList<Integer> match){
        int black = 0;
        int white = 0;
        ArrayList<Integer> copyMatch = new ArrayList<>(match);
        for(int i=3; i>=0 ;i--){
            int color = boardButtons[i].getId();
            if(copyMatch.contains(color)){
                int index = copyMatch.indexOf(color);
                if(index==i){
                    black += 1;
                } else{
                    white += 1;
                }
                copyMatch.set(index,0);
            }
        }
        return new Pair<>(black,white);
    }
}
