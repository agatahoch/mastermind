package com.example.agata.mastermind;


import android.util.Pair;
import android.widget.Button;

import java.util.ArrayList;

public class BoardTools {
    public static void changeRow(int count,boolean enabled,Button[][] boardButtons){
        for (Button boardButton:boardButtons[count]) {
            boardButton.setEnabled(enabled);
        }
    }
    public static void gradeRow(Pair<Integer,Integer> pair, Button[][] checkButtons){
        int black = pair.first;
        int white = pair.second;

        ArrayList<Integer> list = new ArrayList<>();

        for(int i=0;i<black;i++){
            list.add(R.color.black);
        }
        for(int i=0;i<white;i++){
            list.add(R.color.white);
        }
        int size = list.size();
        for(int i=0;i<4-size;i++){
            list.add(0);
        }
        int k = 0;
        for(int i=0;i<2;i++){
            for(int j=0;j<2;j++){
                checkButtons[i][j].setBackgroundResource(list.get(k));
                k += 1;
            }
        }
    }
}
