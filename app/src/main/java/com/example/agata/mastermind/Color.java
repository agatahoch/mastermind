package com.example.agata.mastermind;


public enum Color {
    RED,
    YELLOW,
    BLUE,
    GREEN,
    ORANGE,
    PURPLE
}
